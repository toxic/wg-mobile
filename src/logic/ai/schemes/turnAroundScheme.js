wg.namespace("wg.logic.ai.schemes");

(function()
{
var TurnAroundScheme = function(parent, params)
{
    this.parent = parent;
    this.turnDelay = cc.rand() % 15;
}

TurnAroundScheme.prototype.update = function(deltaTime)
{
    if(this.parent.group == 'players')
    {
        return;
    }


    this.turnDelay -= deltaTime;
    if(this.turnDelay < 0)
    {
        this.turnDelay = cc.rand() % 15;
        this.parent.controller.setTargetAngle(cc.rand() % 360);
    }


}

wg.logic.ai.schemes.TurnAroundScheme = TurnAroundScheme;

})();


