wg.namespace("wg.logic.ai.behaviors");

(function(){

var Behavior = function(controller)
{
    this.controller = controller;
    this.wanderForceLength = 1000;


    this.wanderTime = 1;
    this.wanderDelay = 0;
}

Behavior.prototype.update = function(deltaTime)
{

    if(this.wanderDelay > 0)
    {
        this.wanderDelay -= deltaTime;

        if(this.wanderDelay <= 0)
        {
            this.wanderTime = cc.rand() % 15;
        }
    }

    else if(this.wanderTime > 0)
    {
        var wanderForce = cc.p(cc.rand() % 10 - 5 , cc.rand() % 10 - 5);
        cc.pNormalizeIn(wanderForce);
        this.controller.addSteering(wanderForce, this.wanderForceLength);

        this.wanderTime -= deltaTime;
        if(this.wanderTime <= 0)
        {
            this.wanderDelay = cc.rand() % 5;
        }
    }

}

wg.logic.ai.behaviors.Wander = Behavior;


})();