wg.namespace("wg.logic.ai.behaviors");

(function(){


var Behavior = function(controller)
{
    this.controller = controller;
    this.scanRangeSq = 100 * 100;
};


Behavior.prototype.computeDoodadForce = function(doodad, position, radius)
{
    var direction = cc.pSub(doodad.position, position);
    var distance = cc.pLengthSQ(direction) - (doodad.physics.radius + radius) * (doodad.physics.radius + radius);

    if(distance < this.scanRangeSq)
    {
        var force = distance / this.scanRangeSq * 50;
        cc.pNormalize(direction);
        cc.pMultIn(direction, -force);
        return direction;
    }

    return cc.p(0,0);
}


Behavior.prototype.update = function(deltaTime)
{
    var steeringForce = cc.p(0, 0);
    var nearbyStaticColliders = wg.game.gameLayer.mapLayer.doodadCollisionCandidates(this.controller.parent.position);
    var position = this.controller.parent.position;
    var radius = this.controller.parent.physics.radius;

    var that = this;
    nearbyStaticColliders.forEach(function(staticCollider) {
        cc.pAddIn(steeringForce, that.computeDoodadForce(staticCollider, position, radius));
    });

    cc.pNormalize(steeringForce);
    this.controller.addSteering(steeringForce, 1);

}

wg.logic.ai.behaviors.AvoidTerrain = Behavior;

})();