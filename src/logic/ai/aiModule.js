wg.namespace("wg.logic.ai");

(function()
{

var Ai = function(parent, params)
{
    this.parent = parent;
    this.currentScheme = new wg.logic.ai.schemes.TurnAroundScheme(parent);
}

Ai.prototype.init = function()
{

}

Ai.prototype.update = function(deltaTime)
{
    this.currentScheme.update(deltaTime);
}

wg.logic.ai.Ai = Ai;

})();


