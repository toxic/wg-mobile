wg.namespace("wg.c");

(function()
{
    var COLLISION_LAYERS = {

        NONE: 0,

        // 1/2 player radius or less, ex. mouses
        SMALL: 1 << 0,
        // roughly player size
        MEDIUM: 1 << 1,
        // twice player size 
        LARGE: 1 << 2,
        //  mb bosses?
        GIANT: 1 << 3,
        // wasps
        FLYING: 1 << 4
    };

    wg.c.COLLISION_LAYERS = COLLISION_LAYERS;

})();