wg.namespace("wg.c");
wg.namespace("wg.logic.physics");

(function()
{
var Physics = function(parent, params)
{
    this.parent = parent;
    this.oldPosition = cc.p(0, 0);

    // current velocity of unit
    this.velocity = cc.p(0, 0);
    // mass - multipled by speed is used to calculate collision response
    this.mass = 1;
    // collision radius
    this.radius = 0;
    // object acts as if it was infinitely heavy (can't be pushed by other obejct)
    this.moveable = true;
    // if fales only collision handlers will trigger
    this.collisionResponseEnabled = true;
    this.collideWithTerrain = true;
    // layers that object will collide with
    this.collisionLayers = wg.c.COLLISION_LAYERS.MEDIUM;
    // layers for which object will check for collision
    this.collideWithLayer = wg.c.COLLISION_LAYERS.MEDIUM;
    // speed, that object can accelerate itself on its own
    this.moveSpeed = 0;
    // object acceleration in speed units / second
    this.acceleration = 600000;

    for (var key in params) {
        this[key] = params[key];
    }
}

Physics.prototype.init = function()
{
}

Physics.prototype.update = function(deltaTime)
{
    if(this.moveable)
    {
        cc.pAddIn(this.parent.position, cc.pMult(this.velocity, deltaTime));
    }

    var that = this;
    var staticCollisionCandidates = wg.game.gameLayer.mapLayer.doodadCollisionCandidates(this.parent.position);
    staticCollisionCandidates.forEach(function(collisionCandidate) {
        wg.logic.physics.checkCollision(that.parent, collisionCandidate, deltaTime);
    });

    if(this.collisionBucketCoords !== undefined)
    {
        var nearbyBuckets = wg.game.gameLayer.collisionManager.getNearbyBuckets(this.collisionBucketCoords);
        nearbyBuckets.forEach(function(entityList) {
            entityList.forEach(function(entity){
                if(entity != that.parent)
                {
                    wg.logic.physics.checkCollision(that.parent, entity, deltaTime);
                }
            });
        });
    }
    wg.game.gameLayer.collisionManager.update(this.parent);
}

wg.logic.physics.Physics = Physics;
})();