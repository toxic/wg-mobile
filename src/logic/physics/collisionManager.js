wg.namespace("wg.logic.physics");

(function()
{
// this object is responsible for managing colliding entities
// grid space partitioning is used to reduce amount of collision tests
var CollisionManager = function(width, height, gridSize)
{
    this.width = width/gridSize;
    this.height = height/gridSize;
    this.gridSize = gridSize;

    this.maxIndex = this.width * this.height - 1;

    this.grid = [];

    for(var y = 0; y < this.height; y++)
    {
        var row = [];
        this.grid.push(row);
        for(var x = 0; x < this.width; x++)
        {
            row.push([]);            
        }
    }
}

CollisionManager.prototype.collisionBucketCoord = function(position)
{
    var x = Math.floor(position.x / this.gridSize);
    var y = this.height - 1 -Math.floor(position.y / this.gridSize);

    return y * this.width + x;
}

CollisionManager.prototype.getBucket = function(gridIndex)
{
    return this.grid.get(Math.floor(gridIndex/this.width), Math.floor(gridIndex % this.width));
}

CollisionManager.prototype.getNearbyBuckets = function(gridIndex)
{
    var x = Math.floor(gridIndex % this.width)
    var y = Math.floor(gridIndex/this.width);

    var iX = [0, 1, 1, 0, -1, -1, -1, 0, 1];
    var iY = [0, 0, 1, 1, 1, 0, -1, -1, -1];

    var bucketArray = [];
    for(var i = 0; i < 9; i++)
    {
        var newX = x + iX[i];
        var newY = y + iY[i];
        
        var bucket = this.grid.get(newY, newX);
        if(bucket !== undefined)
        {
            bucketArray.push(bucket);
        }
    }

    return bucketArray;
}

// attach/detach/update should be called by physics object only
// Add entity to collision tracking system
CollisionManager.prototype.attach = function(entity)
{
    entity.physics.collisionBucketCoords = this.collisionBucketCoord(entity.position);
    var entityBucket = this.getBucket(entity.physics.collisionBucketCoords);
    if(entityBucket !== undefined)
    {
        entity.physics.collisionBucket = entityBucket;
        entity.physics.collisionBucketCoords = entity.physics.collisionBucketCoords;
        entity.physics.collisionBucket.push(entity);
    }
}

// Remove entity from collision tracking system
CollisionManager.prototype.detach = function(entity)
{
    if(entity.physics.collisionBucket !== undefined)
    {
        entity.physics.collisionBucket.splice(entity.physics.collisionBucket.indexOf(entity), 1);
        entity.physics.collisionBucketCoords = undefined;
        entity.physics.collisionBucket = undefined;
    }
}

// update entity data in collision tracking system
CollisionManager.prototype.update = function(entity)
{
    var newCoord = this.collisionBucketCoord(entity.position);
    if(newCoord != entity.physics.collisionBucketCoords)
    {
        this.detach(entity);
        this.attach(entity);
    }

}

wg.logic.physics.CollisionManager = CollisionManager;
})();