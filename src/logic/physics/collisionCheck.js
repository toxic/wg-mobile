wg.namespace("wg.logic.physics");

(function()
{

wg.logic.physics.checkCollision = function(a, b, deltaTime)
{
    var ap = a.physics;
    var bp = b.physics;
    
    if(ap.collisionLayers & bp.collideWithLayer || ap.collideWithLayer & bp.collisionLayers)
    {
        var distanceSquared = cc.pDistanceSQ(a.position, b.position);
        if(distanceSquared <= (ap.radius + bp.radius) * 
                                (ap.radius + bp.radius))
        {
            // get collisionVector
            var distance = Math.sqrt(distanceSquared);
            var overlapDistance = ap.radius + bp.radius - distance;
            if(overlapDistance > 0)
            {
                if(ap.collisionResponseEnabled && bp.collisionResponseEnabled)
                {
                    if(ap.moveable || bp.moveable)
                    {
                        if(distance != 0)
                        {
                            var abVec = cc.pMult(cc.pSub(a.position, b.position), overlapDistance / distance);
                        }
                        else
                        {
                            // move bodies apart in random direction
                            var abVec = cc.pForAngle(cc.rand());
                        }

                        if(!ap.moveable)
                        {
                            var moveFactor = 1;
                        }
                        else if(!bp.moveable)
                        {
                            var moveFactor = 0;
                        }
                        else
                        {
                            var moveFactor = ap.mass / (ap.mass + bp.mass);
                        }

                        // this is done to prevent bodies from exploding when placed on each other
                        // modify this if bodies start entering each others
                        if(cc.pLengthSQ(abVec) > 5 * 5)
                        {
                            abVec = cc.pNormalize(abVec);
                        }

                        if(overlapDistance < 1e-10)
                        {
                            // get over numerical unstability of numbers
                            // without this enemies who collided once would trigger on collision every frame
                            var epsilon = 1e-10;
                            var direction = cc.pNormalize(abVec);
                            cc.pMultIn(direction, epsilon)
                            direction = cc.pAddIn(abVec, direction);
                        }

                        

                        // move bodies to not collide anymore
                        cc.pAddIn(a.position, cc.pMult(abVec, 1 - moveFactor));
                        cc.pAddIn(b.position, cc.pMult(cc.pMult(abVec, -1), moveFactor));
                    }
                }


                // fire entities specific handlers
                if (typeof a.onCollision === "function") {
                    a.onCollision(b);
                }

                if (typeof b.onCollision === "function") {
                    b.onCollision(a);
                }
            }
        }
    }

}


})();


