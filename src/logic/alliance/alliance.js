wg.namespace("wg.logic.alliances");
wg.namespace("wg.c");

(function()
{

    var Alliance = function(name)
    {
        this.name = name;
        this.enemies = {};
    }

    Alliance.prototype.addEnemy = function(enemyAllianceName)
    {
        this.enemies[enemyAllianceName] = true;
    }

    wg.logic.alliances.get = function(allianceName)
    {
        allianceName = allianceName || 'neutral';

        return alliancesSet[allianceName];
    }

    var alliancesSet = {
        neutral: new Alliance('neutral'),
        player: new Alliance('player'),
        hostile: new Alliance('hostile'),
    };

    alliancesSet.player.addEnemy('hostile');
    alliancesSet.hostile.addEnemy('player');

    wg.logic.alliances.alliancesSet = alliancesSet;

})();


