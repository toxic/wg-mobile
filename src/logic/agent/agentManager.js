wg.namespace("wg.logic.agents");

/*
 * Actors - i.e. monsters, npcs, rats. Something that can have AI, that can move, etc.
 */
(function() {
    //na poczatek demon
    wg.logic.agents.create = function(x, y) {
        var demon = wg.logic.entities.create({
            position: cc.p(x, y),
            group: "enemies",
            physics: {
            mass: 300,
            radius: 28},
            sprite: {
                fileName: res.demon,
                rect: cc.rect(0, 0, 64, 64),
                zOrder: 0.6
            },
            alliance: 'hostile',
            //ai: {},
            controller: {
                modules: {
                    wander: wg.logic.ai.behaviors.Wander,
                    avoidTerrain: wg.logic.ai.behaviors.AvoidTerrain,
                }
            },
        });
        return demon;
    };
})();
