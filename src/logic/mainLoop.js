wg.namespace("wg.logic");

(function() {


    var accumulatedTime = 0.0;
    var logicStep = 1/60;

    wg.logic.update = function(deltaTime) {
        accumulatedTime += deltaTime;
        while (accumulatedTime > logicStep) {
            executeOneStep();
            accumulatedTime -= logicStep;
        }
    }

    executeOneStep = function() {
        //input:
        wg.logic.characterController.update(logicStep);

        //logic:
        wg.logic.projectiles.update(logicStep);
        wg.logic.entities.update(logicStep);

        //post-logic:
        wg.game.gameLayer.camera.update();
    }
})();


