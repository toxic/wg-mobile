wg.namespace("wg.logic.projectiles");

//do przeniesienia w jakies utils.js czy cos
function extend(a, b){
    for(var key in b)
        if(b.hasOwnProperty(key))
            a[key] = b[key];
    return a;
}

(function() {
    var projectiles = wg.logic.projectiles;

    var PROJECTILES_GROUP = "projectiles";
    var ENEMIES_GROUP = "enemies";
    var PLAYERS_GROUP = "players";

    projectiles.create = function(params) {
        var projectile = wg.logic.entities.create(extend(params, {
            group: PROJECTILES_GROUP,
            collidesWith: ENEMIES_GROUP,
            angle: params.rotation,
            position: cc.p(params.position),
            physics: {
                collisionResponseEnabled: false,
                collisionLayers: wg.c.COLLISION_LAYERS.NONE,
                collidesWith: wg.c.COLLISION_LAYERS.NONE,
            },
        }));
        projectile.source = params.source;
        projectile.duration = 1.5;
        projectile.onCollision = function(otherEntity) {
            if(otherEntity.isTerrain)
            {
                projectile.remove();                
            }
            else
            {
                if(this.source.isEnemy(otherEntity))
                {
                    otherEntity.remove();
                    projectile.remove();   
                }
            }
        };
        return projectile;
    };

    projectiles.update = function(deltaTime) {
        wg.logic.entities.getGroup(PROJECTILES_GROUP).forEach(function(projectile) {
            projectile.duration -= deltaTime;
            if (projectile.duration <= 0) {
                wg.logic.entities.remove(projectile);
                return; //continue looping
            }
            var direction = cc.pForAngle(-cc.degreesToRadians(projectile.sprite.rotation - 90));
            cc.pAddIn(projectile.position, cc.pMult(direction, 300 * deltaTime));
        });
    };
})();


