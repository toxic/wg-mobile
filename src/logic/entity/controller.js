wg.namespace("wg.logic.entity");

(function(){

var Controller = function(entity, params)
{
    this.parent = entity;
    this.steering = cc.p(0, 0);
    this.modules = [];
    
    this.settings = {
        turnSpeed: 36,
        maxSpeed: 150, // max speed that entity can achieve
        // force with which entity can influence its own movement
        // higer value, means that entity is more manoeuvrable
        maxSteering: 1000, 
    }
    

    for (var key in params) {
        if(key == 'settings')
        {
            for (var setting in params[key])
            {
                this.settings[setting] = params[key][setting];
            }
        } else if(key == 'modules')
        {
            for (var mod in params[key])
            {
                this.modules.push(new params[key][mod](this));
            }
        }
        else
        {
            this[key] = params[key];
        }
    }

    this.maxSpeedSq = this.settings.maxSpeed * this.settings.maxSpeed;
    this.maxSteeringSq = this.settings.maxSteering * this.settings.maxSteering;


}

Controller.prototype.init = function()
{
    this.setTargetAngle(this.getAngle());

}

Controller.prototype.addSteering = function(force, forceLength)
{
    if(forceLength)
    {
        cc.pMultIn(force, forceLength);
    }
    cc.pAddIn(this.steering, force);
}

Controller.prototype.update = function(deltaTime)
{
    this.steering = cc.p(0, 0);
    
    // update steering behaviors
    for(var i = this.modules.length - 1; i >=0 ; i--)
    {
        this.modules[i].update(deltaTime);
    }

    this.turnAround(deltaTime);

    if(this.steering.x != 0 || this.steering.y != 0) // apply ai to physics

    {        
        var velocity = this.parent.physics.velocity;

        // calculate steering / mass
        if(cc.pLengthSQ(this.steering) > this.maxSteeringSq)
        {
            cc.pNormalizeIn(this.steering);
            cc.pMultIn(this.steering, this.settings.maxSteering / this.parent.physics.mass);
        }
        else
        {
            cc.pMultIn(this.steering, 1/ this.parent.physics.mass);
        }

        // apply steering to velocity
        //cc.pMultIn(this.steering, deltaTime);
        cc.pAddIn(velocity, this.steering);
        if(cc.pLengthSQ(velocity) > this.maxSpeedSq)
        {
            cc.pNormalizeIn(velocity);
            cc.pMultIn(velocity, this.settings.maxSpeed);
        }
        this.parent.physics.velocity = velocity;
    this.setTargetAngle(cc.pAngle(velocity, cc.p(0, 1)));
    }
}

Controller.prototype.turnAround = function(deltaTime)
{
    if(Math.abs(this.getAngle() - this.getTargetAngle()) > 0.001)
    {
        var turnTick = this.settings.turnSpeed * deltaTime;
        var diff = this.getTargetAngle() - this.getAngle();
        var sign = 1;
        if(diff < 0 )
        {
            diff = Math.abs(diff);
            sign = -1;

        }
        if(diff > 180)
        {
            sign = -sign;
        }

        if(diff < turnTick)
        {
            this.setAngle(this.getTargetAngle());
        }
        else
        {
            this.setAngle(this.getAngle() + turnTick * sign);
        }

        return true;
    }
    return false;
}

Controller.prototype.setTargetAngle = function(angle)
{
    this.targetAngle = (angle + 360) % 360;
}

Controller.prototype.getTargetAngle = function()
{
    return this.targetAngle;
}

Controller.prototype.setAngle = function(angle)
{
    this.parent.angle = (angle + 360) % 360;
}

Controller.prototype.getAngle = function()
{
    return this.parent.angle;
}

wg.logic.entity.Controller = Controller;

})();