wg.namespace("wg.logic.entities");
wg.namespace("wg.logic.physics");


(function()
{
var modules = {
    'physics' : wg.logic.physics.Physics,
    'ai': wg.logic.ai.Ai,
    'controller': wg.logic.entity.Controller,
    };

var Entity = function(params)
{
    this.position = cc.p(0, 0);
    this.angle = cc.rand(360);

    for (var key in params) {
        if(modules[key] === undefined)
        {
            this[key] = params[key];    
        }
        else
        {
            this[key] = new modules[key](this, params[key]);
        }
    }

    this.alliance = wg.logic.alliances.get(params.alliance);


    for (var key in modules)
    {
        if(this[key])
        {
            this[key].init();
        }
    }

    checkEntityInitErrors(this);
}
wg.logic.entities.Entity = Entity;

Entity.prototype.update = function(deltaTime) {
    updateModule(this, 'controller', deltaTime);
    updateModule(this, 'physics', deltaTime);
    updateModule(this, 'ai', deltaTime);
}

Entity.prototype.updateGraphics = function(deltaTime) {
    if(this.sprite) {
        this.sprite.setRotation(this.angle);
        this.sprite.setPosition(this.position); 
    }    
}

//convienence method
Entity.prototype.remove = function() {
    wg.logic.entities.remove(this);
}

/**
 * returns normalized vector pointing towards front of the entity
 */
// a moze to nazwac getDirection()?
Entity.prototype.getForward = function() {
    if (this.sprite) {
        return cc.pForAngle(-cc.degreesToRadians(this.angle - 90));
    } else {
        return cc.p(0, 0);
    }
}

//tox, notka do siebie: wywalic to poza Entity
Entity.prototype.isEnemy = function(entity)
{
    return this.alliance.enemies[entity.alliance.name] || false;
}


var updateModule = function(entity, name, deltaTime)
{
    if(entity[name])
    entity[name].update(deltaTime);
}

var checkEntityInitErrors = function(entity)
{
    if(entity.ai && entity.controller === undefined)
    {
        cc.warn('Entity has Ai, but no controller!');
    }

    if(entity.controller && !entity.physics)
    {
        cc.warn('Entity has controller, but no physics!');
    }
}

})();


