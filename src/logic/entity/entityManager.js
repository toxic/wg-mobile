wg.namespace("wg.logic.entities");

/*
 * Entity manager - let it be responsible for creation, removal of
 * game entities (like player, enemies, items, etc).
 *
 * These entities later will be handled by other managers/handlers
 */
(function() {


    
    //@VisibleForDevelopment:
    var entitiesToRemove =[];

    wg.logic.entities.create = function(params) {
        var entity = new wg.logic.entities.Entity(params);

        // attach sprite
        var paramSprite = params.sprite;
        if (paramSprite) {
            entity.sprite = new cc.Sprite(paramSprite.fileName, paramSprite.rect);
            entity.sprite.setPosition(entity.position);
            wg.game.mapLayer.addChild(entity.sprite, paramSprite.zOrder);
            if (paramSprite.animation) {
                var spriteFrames = [];
                if (paramSprite.animation.frames) {
                    paramSprite.animation.frames.forEach(function(frame){
                       spriteFrames.push(new cc.SpriteFrame(paramSprite.fileName, frame));
                    });
                }
                var animate = new cc.Animate(new cc.Animation(spriteFrames, 0.1));
                animate.retain();
/* to nie dziala, bo chyba nie mamy za bardzo oddzielonego sprajta/noda od logiki...
                if (paramSprite.animation.rotate) {
                    var repeat = cc.repeatForever(cc.rotateBy(1.0, 360));
                    entity.sprite.runAction(repeat);
                } */

                var repeatForever = new cc.RepeatForever(animate);
                repeatForever.retain();
                entity.sprite.runAction(repeatForever);

            }
        }

        // add to arrays
        var group = params.group;
        if (group) {
            if(group == 'entities' || group == 'removed')
            {
                cc.warn('Reserved group name: ' + group);
            }
            register(entity, group);
        }

        if(entity.physics)
        {
            wg.game.gameLayer.collisionManager.attach(entity);
        }

        register(entity, 'entitiesArray');
        return entity;
    };
        
    wg.logic.entities.remove = function(entity) {
        entitiesToRemove.push(entity);
    };

    wg.logic.entities.getGroup = function(groupName) {
        return arrays[groupName] || [];
    };

    wg.logic.entities.update = function(deltaTime) {
        arrays.entitiesArray.forEach(function(entity){
            entity.update(deltaTime);
        });
        arrays.entitiesArray.forEach(function(entity){
            entity.updateGraphics(deltaTime);
        });

        _cleanUpEntitiesToRemove();
    };
    
    var _cleanUpEntitiesToRemove = function()
    {
        entitiesToRemove.forEach(function(entity){
            //guard for already removed entities:
            if (entity.entityManagerCache.removed == true) {
                return; //already removed
            }

            entity.entityManagerCache.removed = true;
            if (entity.sprite) {
                wg.game.mapLayer.removeChild(entity.sprite);
            }
            if(entity.physics)
            {
                wg.game.gameLayer.collisionManager.detach(entity);
            }
            for (var array in entity.entityManagerCache) {
                if(array != 'removed')
                {
                    unregister(entity, array);
                }
            };
        });
    }

    var arrays = {
        entities : [],
    }
    var entitiesArray = arrays.entitiesArray;
    var register = function(entity, key)
    {
        arrays[key] = arrays[key] || [];

        entity.entityManagerCache = entity.entityManagerCache || { removed: false };
        entity.entityManagerCache[key] = arrays[key].length;
        arrays[key].push(entity);
    }
    var unregister = function(entity, key)
    {
        var lastItem = arrays[key][arrays[key].length-1];
        lastItem.entityManagerCache[key] = entity.entityManagerCache[key];
        arrays[key][lastItem.entityManagerCache[key]] = lastItem;
        arrays[key].pop();
    }
    wg.logic.entities.entitiesArray = arrays['entities'];
})();
