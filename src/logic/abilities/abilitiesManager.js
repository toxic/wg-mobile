wg.namespace("wg.logic.abilities");

(function() {
    wg.logic.abilities.byName = function(name) {
        return abilities[name] || {};
    }

    var abilities = {
        "firebolt": {
            effect: {
                projectile: {
                    sprite: {
                        fileName: "res/projectiles/firebolt.png",
                        rect: cc.rect(0, 0, 24, 24),
                        zOrder: 0.6,
                        animation : {
                            frames: [
                                cc.rect(0, 0, 24, 24),
                                cc.rect(0, 25, 24, 24),
                                cc.rect(0, 48, 24, 24)
                            ]
                        }
                    }
                }
            },
            coolDown: 0.5
        },
        "lightning-bolt": {
            effect: {
                projectile: {
                    sprite: {
                        fileName: "res/projectiles/electric-spark.png",
                        rect: cc.rect(0, 0, 16, 16),
                        zOrder: 0.6,
                        animation: {
                            frames: [
                                cc.rect(0, 0, 16, 16),
                                cc.rect(16, 0, 16, 16),
                                cc.rect(32, 0, 16, 16)
                            ]
                        }
                    }
                }
            },
            coolDown: 0.2
        },
        "fireball": {
            effect: {
                projectile: {
                    sprite: {
                        fileName: "res/projectiles/fireball.png",
                        rect: cc.rect(0, 0, 30, 30),
                        zOrder: 0.6,
                        animation: {
                            rotate: true
                        }
                    }
                }
            },
            coolDown: 1.2
        }
    };
})();
