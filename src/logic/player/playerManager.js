wg.namespace("wg.controls");
wg.namespace("wg.logic.players");

(function() {
    wg.logic.players.create = function (x, y) {
        var player = wg.logic.entities.create({
            group: "players",
            sprite: {
                fileName: res.player,
                rect: cc.rect(0, 0, 64, 64),
                zOrder: 1
            },
            position: cc.p(x, y),
            physics: {
                mass: 70,
                radius: 28,
            },
            controller: {
                settings: {
                    turnSpeed: 360,
                },
                modules: {
                    playerController: wg.logic.players.playerControllerBehavior,
                }
            },
            alliance: 'player',
            abilities: []
        });

        //@VisibleForDevelopment:
        wg.logic.player = player;

        //na szybko:
        player.abilities.push(wg.logic.abilities.byName("firebolt"));
        player.abilities.push(wg.logic.abilities.byName("lightning-bolt"));
        player.abilities.push(wg.logic.abilities.byName("fireball"));

        var animate = new cc.Animate(new cc.Animation([
            new cc.SpriteFrame(res.player, cc.rect(64 * 0, 0, 64, 64)),
            new cc.SpriteFrame(res.player, cc.rect(64 * 1, 0, 64, 64)),
            new cc.SpriteFrame(res.player, cc.rect(64 * 2, 0, 64, 64)),
            new cc.SpriteFrame(res.player, cc.rect(64 * 3, 0, 64, 64)),
            new cc.SpriteFrame(res.player, cc.rect(64 * 4, 0, 64, 64)),
            new cc.SpriteFrame(res.player, cc.rect(64 * 5, 0, 64, 64)),
            new cc.SpriteFrame(res.player, cc.rect(64 * 6, 0, 64, 64)),
            new cc.SpriteFrame(res.player, cc.rect(64 * 7, 0, 64, 64))
        ], 0.1));
        animate.retain();
        var repeatForever = new cc.RepeatForever(animate);
        repeatForever.retain();
        player.movementAction = new cc.Speed(repeatForever, 1.0);
        player.movementAction.retain();

        return player;
    };
})();

