wg.namespace("wg.logic");

wg.logic.characterController = {
    abilitiesState: [{}, {}, {}, {}],
    attach: function(player) {
        this.player = player;
    },

    startAbility: function(index) {
        this.abilitiesState[index].isActive = true;
    },
    stopAbility: function(index) {
        this.abilitiesState[index].isActive = false;
    },

    updateAbilitiesState: function(deltaTime, player) {
        for (var i = 0; i < 4; i++) {
            var abilityState = this.abilitiesState[i];
            var ability = player.abilities[i];
            if (!ability) {
                continue;
            }

            var coolDown = abilityState.coolDown;
            if (coolDown) {
                coolDown -= deltaTime;
            }
            abilityState.coolDown = (coolDown <= 0)
                    ? undefined
                    : coolDown;

            var effect = ability.effect;
            if (abilityState.isActive) {
                if (effect.projectile) {
                    if (!abilityState.coolDown) {
                        var projectile = wg.logic.projectiles.create({
                            position: player.position,
                            rotation: player.sprite.getRotation(), //todo: let the player/playercontroller keep the rotation
                            source: player,
                            sprite: effect.projectile.sprite
                        });
                        abilityState.coolDown = ability.coolDown;
                    }
                }
            }
        }
    },

    update: function(deltaTime) {
        var player = this.player;
        if (!player) {
            return;
        }
        this.updateAbilitiesState(deltaTime, player);
        var sprite = player.sprite;
        var deltaMovement = wg.game.controls.movementJoystick.value;
        var rotateTresholdSQ = 0.0 * 0.0; // usunac jesli niepotrzebne
        var clearAction = true;
        if (cc.pLengthSQ(deltaMovement) > rotateTresholdSQ) {
            var movementTresholdSQ = 0.95 * 0.95;
            if (cc.pLengthSQ(deltaMovement) > movementTresholdSQ) {
                clearAction = false;
                if (!player.runningAction) {
                    player.runningAction = sprite.runAction(player.movementAction);
                }
                player.runningAction.setSpeed(cc.pLength(deltaMovement) * 1.33);
            }
        }
        if (clearAction && player.runningAction) {
            sprite.stopAction(player.runningAction);
            player.runningAction = undefined;
        }
    }
}
