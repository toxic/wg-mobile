wg.namespace("wg.logic.players");

(function() {

var Behavior = function(controller)
{
    this.controller = controller;

    this.update = function(deltaTime)
    {
        var deltaMovement = wg.game.controls.movementJoystick.value;
        var rotateTresholdSQ = 0.0 * 0.0; // usunac jesli niepotrzebne
        if (cc.pLengthSQ(deltaMovement) > rotateTresholdSQ) {
            this.controller.setTargetAngle(90 + cc.radiansToDegrees(-cc.pToAngle(deltaMovement)));
            var movementTresholdSQ = 0.95 * 0.95;
            if (cc.pLengthSQ(deltaMovement) > movementTresholdSQ) {
                this.controller.steering = cc.pMult(deltaMovement, 1500);
            }
        }
        else
        {
            this.controller.steering = cc.pMult(this.controller.parent.physics.velocity, - 10);
            this.controller.setTargetAngle(this.controller.getAngle());
        }
    }
}
wg.logic.players.playerControllerBehavior = Behavior;

})();