(function(){

    // get element from multidimensional array
    // or undefined if it doesn't exists
    Array.prototype.get = function()
    {
        var array = this;
        for(var i = 0; i < arguments.length; i++)
        {
            var array = array[arguments[i]];
            if(array === undefined)
            {
                return undefined;
            }
        }

        return array;
    }

    Array.prototype.clone2DArray = function()
    {
        var height = this.length;
        var newArray = [];

        for(var y = 0; y < height; y++)
        {
            newArray.push(this[y].slice(0));
        }
        
        return newArray;
    }

    Math.PI2 = Math.PI * 2;

    wg.util.foreach = function(array, func, border)
    {
        border = border || 0;
        var height = array.length;
        var width = array[0].length;
        for(var y = border; y < height - border; y++)
        {
            for(var x = border; x < width - border; x++)
            {
                func(array, x, y);
            }
        }
    }

    wg.util.randomInt = function(seedrandom, min, max)
    {
        var result = Math.round(seedrandom() * 100000);
        return ((result % (max - min)) + min);       
    }


    wg.util.sign = function(number)
    {
        return number?number<0?-1:1:0;
    }





})();