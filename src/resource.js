var res = {
    HelloWorld_png: "res/HelloWorld.png",
    CloseNormal_png: "res/CloseNormal.png",
    CloseSelected_png: "res/CloseSelected.png",
    OldMenuMockup_png: "res/OldMenuMockup.png",
    player: "res/player.png",
    joystick_base: "res/joystick/base.png",
    joystick_stick: "res/joystick/stick.png",
    button_base: "res/ui/button_base.png",
    button_base_pressed: "res/ui/button_base_pressed.png",
    firebolt: "res/projectiles/firebolt.png",
    demon: "res/actors/demon.png"
};

var res_path = {
    shaders: "res/shaders/",
    maps: "res/assets/maps/tiles/",
    doodads: "res/assets/maps/doodads",
}

var g_resources = [];
for (var i in res) {
    g_resources.push(res[i]);
}

var doodadDefinitions = // może się przyda później
{
    jungle : [
    "leaves3.png",
    "purchl.png",
    "sponge.png",
    "stick1.png",
    "stick2.png",
    "sticks1.png",
    "sticks2.png",
    "sticks3.png",
    "treebark1.png",
    "treebark2.png",
    "bush1.png",
    "bush2.png",
    "bush3.png",
    "bush4.png",
    "conopy.png",
    "flatbush01.png",
    "flatbush02.png",
    "flatbush03.png",
    "flatbush04.png",
    "flatbush05.png",
    "flatbush06.png",
    "flatbush07.png",
    "flatbush08.png",
    "flatbush09.png",
    "flatbush10.png",
    "flatbush11.png",
    "flatbush12.png",
    "flatbush13.png",
    "hole.png",
    "leaves1.png",
    "leaves2.png"]
};