(function () {

var gen = {};
wg.map.MapGenerator.prototype.generateMapCellurarAutomaton = function()
{
    var blank = 0;

    var wall = this.wall;
    var ground = this.ground;
    
    gen = this;
    this._prepareMap(); // clear map
    var map = this.map.data;

    // randomize walls initial
    this.foreach(map, function(map, x, y)
    {
        if(gen.randomInt(100) < 50)
        {
            map[y][x] = blank;
        }
        else
        {
            map[y][x] = wall;
        }
    },3);

    // vertical and horizontal strips for increased empty space at middle
    for(var y = 3; y < this.height - 3; y++)
    {
        map[y][this.width/2] = blank;
        map[y][this.width/2 + 1] = blank;
        map[y][this.width/2 - 1] = blank;
    }

    for(var x = 3; x < this.width - 3; x++)
    {
        map[this.height/2][x] = blank;
        map[this.height/2+1][x] = blank;
        map[this.height/2-1][x] = blank;
    }

    // iterate CA
    for(var iteration = 0; iteration < 4; iteration++)
    {
        var copy = map.clone2DArray();
        this.foreach(copy, function(copy, x, y)
        {
            var walls = 0;
            for(var i = 0; i < 9; i++)
            {
                if(map[y + gen.iY[i]][x + gen.iX[i]] != blank)
                {
                    walls++;
                }
            }
            if(walls > 5 || walls < 1)
            {
                copy[y][x] = blank;
            }
            else
            {
                copy[y][x] = wall;
            }
        }, 3);
        map = copy;
    }

    for(var iteration = 0; iteration < 4; iteration++)
    {
        var copy = map.clone2DArray();
        this.foreach(copy, function(copy, x, y)
        {
            var walls = 0;
            for(var i = 0; i < 9; i++)
            {
                if(map[y + gen.iY[i]][x + gen.iX[i]] != blank)
                {
                    walls++;
                }
            }
            if(walls > 5)
            {
                copy[y][x] = blank;
            }
            else
            {
                copy[y][x] = wall;
            }
        }, 3);
        map = copy;
    }

    // convert borders to walls
    for(var y = 0; y < 3; y++)
    {
        for(var x = 0; x < this.width; x++)
        {
            map[y][x] = wall;
            map[this.height -1  - y][x] = wall;
        }
    }

    for(var x = 0; x < 3; x++)
    {
        for(var y = 0; y < this.height; y++)
        {
            map[y][x] = wall;
            map[y][this.width -1 -x] = wall;
        }
    }
        
    // chip edges
    for(var y = 0; y < this.height; y++)
    {
        if(this.random() > 0.5)
        {
            map[y][3] = wall;
        }

        if(this.random() > 0.5)
        {
            map[y][this.width -4] = wall;
        }
    }
    for(var X = 0; x < this.width; x++)
    {
        if(this.random() > 0.5)
        {
            map[3][x] = wall;
        }

        if(this.random() > 0.5)
        {
            map[this.height -4 ][x] = wall;
        }
    }

    // mark groups
    var requiredCells = this.width * this.height / 20;
    var groups = [];
    this.foreach(map, function(map, x, y)
    {
        if(map[y][x] == blank)
        {
            var res = floodFill(map, x, y, ground);
            if(res[0] < requiredCells)
            {
                floodFill(map, x, y, wall); 
            }
            else
            {
                groups.push(res[1]);
            }
        }
    }, 3);
    
    // path groups to middle
    var dest = [this.height/2, this.width/2];
    while(groups.length > 0)
    {
        var pos = groups.pop();
        while(pos[0] != dest[0] || pos[1] != dest[1])
        {
            if(pos[0] < dest[0] || this.random() < 0.3)
            {
                pos[0]++;
            }
            else if(pos[0] > dest[0] || this.random() < 0.3)
            {
                pos[0]--;
            }

            if(pos[1] < dest[1] || this.random() < 0.3)
            {
                pos[1]++;
            }
            else if(pos[1] > dest[1] || this.random() < 0.3)
            {
                pos[1]--;
            }
            
            for(var i = 0; i < 9; i++)
            {
                map[pos[0] + gen.iY[i]][pos[1] + gen.iX[i]] = ground;
            }
        }
                
    }

    this.map.data = map;
    this.map.init();
}

    var floodFill = function(map, startX, startY, replace)
    {
        // find closest position to center
        var closestPos = [startY, startX];
        var center = [map.length/2, map[0].length/2];
        var closestPosDist = map.length * map.length;

        // paint
        var count = 0;
        var seek = map[startY][startX];
        var stack = [];
        stack.push([startY, startX]);
        map[startY][startX] = replace;

        while(stack.length > 0)
        {
            var current = stack.pop();
            map[current]
            for(var i = 1; i < 9; i += 2) // skip diagonal passages
            {
                var x =current[1] + gen.iX[i];
                var y =current[0] + gen.iY[i];
                if(map[y][x] == seek)
                {
                    stack.push([y, x]);
                    map[y][x] = replace;
                    count ++;

                    var distToCenter = (y - center[0]) * (y - center[0]) +
                        (x - center[1]) * (x - center[1]);
                    if(distToCenter < closestPosDist)
                    {
                        closestPosDist = distToCenter;
                        closestPos = [y, x];
                    }
                }
            }

        }
        
        return [count, closestPos];
    }
})();