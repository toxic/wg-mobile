(function () {

var TILE_SIZE = wg.c.TILE_SIZE;
var TREE_RADIUS = wg.c.TREE_RADIUS;
var gen = {};



wg.map.MapGenerator.prototype.generateDoodads = function()
{
    // under player and monsters - grass, bush, rock etc.
    var resultGround = [];
    // over player and characters - tree, roof etc.
    var result = [];
    // array, where 1 = wall, and 0 = stepable ground
    var mapData = this.map.data;
    var map = this.map;
    
    // constants
    var gen = this;
    var height = this.height;
    var width = this.width;
    var wall = this.wall;
    var ground = this.ground;

    // loop
    var scale = 0;
    var xPos = 0;
    var yPos = 0;
    
    function makeDoodadEntity(tileset, asset, zIndex, radius)
    {
        radius = radius || TREE_RADIUS;

        var result = {
            tileset: tileset, 
            name:asset, 

            position: cc.p(xPos * TILE_SIZE + TILE_SIZE/2, yPos * TILE_SIZE + TILE_SIZE/2), 
            rotation: gen.randomInt(359), 
            isTerrain: true,
            scale:scale, 
            zIndex:zIndex};
        result.physics = new wg.logic.physics.Physics(result, {
                velocity: cc.p(0, 0),
                radius: scale * radius,
                moveable: false
            });
        return result;
    }

    function addDoodad(tileset, asset, zIndex, radius)
    {
        radius = radius || TREE_RADIUS;
        
        
        var field = map.fields[Math.round(yPos)][Math.round(xPos)];
        field.doodadsDefinitions.push(makeDoodadEntity(tileset, asset, zIndex, radius));
    }

    function addGroundDoodad(tileset, asset, zIndex)
    {
        var radius = radius || TREE_RADIUS; //<-- to ma sens?

        var field = map.fields[Math.round(yPos)][Math.round(xPos)];
        field.doodadsGroundDefinitions.push(makeDoodadEntity(tileset, asset, zIndex, radius));
    }

    for(var x = 0; x < width; x++)
    {
        for(var y = 0; y < height; y++)
        {
            xPos = x;
            yPos = y;

            // check number of stepable tiles within 3x3 square
            var jungleTilesAround = 0;
            var deepForest = 0;
            // used to move trees into jungle a bit
            var groundToWallVectorX = 1;
            var groundToWallVectorY = 1;
            if(x != 0 && y != 0 && x != width- 1 && y != height - 1)
            {
                for(var i = 0; i < 9; i++)
                {
                    var currentTile = mapData[this.iY[i] + y][this.iX[i] + x];
                    if(currentTile == wall)
                    {
                        jungleTilesAround += 1;
                    }
                    else if(currentTile == ground)
                    {
                        groundToWallVectorX += this.iX[i];
                        groundToWallVectorY += this.iY[i];
                    }
                }

                if(jungleTilesAround == 9)
                {
                    deepForest = 1;
                    var isEvenDeeperForest = true;
                    // check 5x5 square
                    if(x > 1 && y > 1 && x < width- 2 && y < height - 2)
                    for(var dx = -2;  dx <= 2; dx++) 
                    {
                        if(mapData[y + 2][x + dx] != wall || mapData[y - 2][x + dx] != wall)
                        {
                            isEvenDeeperForest = false;
                            break;
                        }
                    }
                    else isEvenDeeperForest = false;
                    if(isEvenDeeperForest)
                    for(var dy = -1;  dy <= 1; dy++) 
                    {
                        if(mapData[y + dy][x + 2] != wall || mapData[y - dy][x + 2] != wall)
                        {
                            isEvenDeeperForest = false;
                            break;
                        }
                    }
                    if(isEvenDeeperForest)
                    {
                        deepForest = 2;
                    }
                }
            }
            else // map border
            {
                jungleTilesAround = 9;
            }

            if(mapData[y][x] == wall)
            {
                if(jungleTilesAround <= 2) // boulder or foliage
                {
                    scale = 2 + (this.randomInt(30) / 100.0);

                    addDoodad("plants", "foliage" + (this.randomInt(6)+1), 0, TREE_RADIUS/2);
                } 
                else if(jungleTilesAround < 9) // forest border
                {
                    if(groundToWallVectorX != 0 || groundToWallVectorY != 0)
                    {
                        groundToWallVectorX += (this.randomInt(10) - 5) / 10.0;
                        groundToWallVectorY += (this.randomInt(10) - 5)/ 10.0;
                        var vecLength = Math.sqrt(groundToWallVectorX*groundToWallVectorX + groundToWallVectorY*groundToWallVectorY);
                        groundToWallVectorX /= vecLength * 3;
                        groundToWallVectorY /= vecLength * 3;
                    }
                    
                    scale = 1.6 + (this.randomInt(15) / 100.0);
                    xPos = x - groundToWallVectorX;
                    yPos = y - groundToWallVectorY;
                    addDoodad("plants", "tree" + (this.randomInt(8)+1), scale);
                }
                else // deep forest
                {
                    if(deepForest == 1)
                    {
                        scale = 1.5 + (this.randomInt(50) / 100.0);
                    }
                    else
                    {
                        scale = 2 + (this.randomInt(150) / 100.0);
                    }
                    addDoodad("plants", "tree" + (this.randomInt(8)+1), scale);
                }
            }
            else if(mapData[y][x] == ground)
            {
                // decorations
                var zIndex = 0;
                if(this.randomInt(100) >= 70) // chance for doodad
                {
                    scale = 1.0 + (this.randomInt(60) / 100.0);
                    var doodadIndex = this.randomInt(7);
                    if(doodadIndex == 0)
                    {
                        addGroundDoodad("plants", "foliage8", zIndex);
                    } else if(doodadIndex == 1)
                    {
                        addGroundDoodad("plants", "foliage9", zIndex);
                    } else if(doodadIndex >= 2 || doodadIndex <= 5)
                    {
                        addGroundDoodad("desert", "leaf", zIndex);
                    } else if(doodadIndex == 6)
                    {
                        addGroundDoodad("desert", "grass1", zIndex);
                    } else if(doodadIndex == 7)
                    {
                        addGroundDoodad("desert", "grass2", zIndex);
                    }
                }


            }
        }
    }

    this.map.groundDoodadData = resultGround;
    this.map.doodadData = result;
};


})();