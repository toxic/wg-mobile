(function () {
    
var MapGenerator = function(seed, width, height)
{
    this.foreach = wg.util.foreach;
    // random number generator
    this.random = new Math.seedrandom(seed);
    this.random();
    this.random();
    this.random();
    

    this.iX = [0, 1, 1, 0, -1, -1, -1, 0, 1];
    this.iY = [0, 0, 1, 1, 1, 0, -1, -1, -1];

    
    this.randomInt = function(x) { return wg.util.randomInt(this.random, 0, x); }

    this.seed = seed;
    this.width = width;
    this.height = height;
    this.map = new wg.map.MapData();

    
    this.wall = 1;
    this.ground = 2;
}

MapGenerator.prototype._prepareMap = function(clear)
{
    clear = clear || 0;

    var data = [];
    for(var y = 0; y < this.height; y++)
    {
        var row = [];
        data[data.length] = row;
        for(var x = 0; x < this.width; x++)
        {
            row[row.length] = clear;
        }
    }

    this.map.data = data;
}

    wg.map.MapGenerator = MapGenerator;
})();