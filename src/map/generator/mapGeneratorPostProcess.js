"use strict";

(function () {
var TILE_SIZE = wg.c.TILE_SIZE;
var gen = {};
wg.map.MapGenerator.prototype.postProcess = function(inputMap, doodadData)
{
    var map = this.map.data;
    var height = this.height;
    var width = this.width;

    var wall = this.wall;
    var ground = this.ground;

    for(var x = 1; x < width - 1; x++)
    {
        for(var y = 1; y < height - 1; y++)
        {
            // fill empty space
            var wallsAround = 0; 
            for(var i = 0; i < 9; i++)
            {
                var currentTile = map[this.iY[i] + y][this.iX[i] + x];
                if(currentTile == wall)
                {
                    wallsAround ++;
                }
            }

            if(wallsAround <= 1 && map[y][x] != wall) // watch out to don't block road
            {
                if(this.randomInt(100) < 10) // chance for freestanding wall
                {
                    map[y][x] = wall;
                }

            }

        }
    }
};

var generatorMaxIterations = 100;

wg.map.MapGenerator.prototype.bottomWay = function()
{
    var map = this.map.data;
    var ground = this.ground;

    for(var i = 0; i < generatorMaxIterations; i++)
    {
        var yPos = this.randomInt(10);
        var xPos = this.randomInt(map[0].length - 2 ) + 1;

        if(map[yPos][xPos] == ground)
        {
            for(var y = yPos; y >= 0; y--)
            {
                map[y][xPos-1] = ground;
                map[y][xPos] = ground;
                map[y][xPos+1] = ground;
            }

            return [xPos, y + 2];
        }
    }
}

wg.map.MapGenerator.prototype.topWay = function()
{
    var map = this.map.data;
    var ground = this.ground;

    for(var i = 0; i < generatorMaxIterations; i++)
    {
        var yPos = map.length - this.randomInt(10) - 1;
        var xPos = this.randomInt(map[0].length - 2 ) + 1;

        if(map[yPos][xPos] == ground)
        {
            for(var y = yPos; y < map.length; y++)
            {
                map[y][xPos-1] = ground;
                map[y][xPos] = ground;
                map[y][xPos+1] = ground;
            }

            return [xPos, y - 2];
        }
    }
}

wg.map.MapGenerator.prototype.leftWay = function()
{
    var map = this.map.data;
    var ground = this.ground;

    for(var i = 0; i < generatorMaxIterations; i++)
    {
        var xPos = this.randomInt(10);
        var yPos = this.randomInt(map.length - 2 ) + 1;

        if(map[yPos][xPos] == ground)
        {
            for(var x = xPos; x >= 0; x--)
            {
                map[yPos-1][x] = ground;
                map[yPos][x] = ground;
                map[yPos+1][x] = ground;
            }

            return [x + 2, yPos];
        }
    }
}

wg.map.MapGenerator.prototype.rightWay = function()
{
    var map = this.map.data;
    var ground = this.ground;

    for(var i = 0; i < generatorMaxIterations; i++)
    {
        var xPos = map[0].length - this.randomInt(10) - 1;
        var yPos = this.randomInt(map.length - 2 ) + 1;

        if(map[yPos][xPos] == ground)
        {
            for(var x = xPos; x < map[0].length; x++)
            {
                map[yPos-1][x] = ground;
                map[yPos][x] = ground;
                map[yPos+1][x] = ground;
            }

            return [x - 2, yPos];
        }
    }
}

var dir = wg.c.DIRECTIONS;
wg.map.MapGenerator.prototype.createExitEntrance = function(entranceDirection, exitDirection)
{
    var wall = this.wall;
    var ground = this.ground;

    this.entrance = null;
    this.exit = null;

    if(entranceDirection == dir.BOTTOM)
    {
        this.entrance = this.bottomWay();
    }
    if(exitDirection == dir.BOTTOM)
    {
        this.exit = this.bottomWay();
    }

    if(entranceDirection == dir.TOP)
    {
        this.entrance = this.topWay();
    }
    if(exitDirection == dir.TOP)
    {
        this.exit = this.topWay();
    }

    if(entranceDirection == dir.LEFT)
    {
        this.entrance = this.leftWay();
    }
    if(exitDirection == dir.LEFT)
    {
        this.exit = this.leftWay();
    }

    if(entranceDirection == dir.RIGHT)
    {
        this.entrance = this.rightWay();
    }
    if(exitDirection == dir.RIGHT)
    {
        this.exit = this.rightWay(this);
    }

    if(this.entrance != null && this.exit != null)
    {
        this.entrance = [this.entrance[0] * TILE_SIZE + TILE_SIZE/2, this.entrance[1] * TILE_SIZE + TILE_SIZE/2];
        this.exit = [this.exit[0] * TILE_SIZE, this.exit[1] * TILE_SIZE];

        return true;    
    }
    else
    {
        //regenerate map
        return false;
    }
}



})();