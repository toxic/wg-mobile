(function()
{
    var TILE_SIZE = wg.c.TILE_SIZE;
    // map that holds tile and rendering data
    var FieldData = (function(mapData, x, y)
    {
        this.visible = false;
        this.x = x;
        this.y = y;
        this.mapData = mapData;
        this.textureIndex = 0; // id of texture in tileData
        this.doodadsDefinitions = []; // overhead doodads
        this.doodadsGroundDefinitions = []; // ground doodads
        this.sprites = []; // all sprites owned by field
    });


    // create sprites
    FieldData.prototype.show = function(mapLayer)
    {
        if(this.visible == false)
        {
            this.visible = true;
            this.sprites.push(this.createTileSprite(mapLayer));

            for(var i = 0; i < this.doodadsGroundDefinitions.length; i++)
            {
                this.sprites.push(this.createDoodadSprite(this.doodadsGroundDefinitions[i], mapLayer));
            }

            for(var i = 0; i < this.doodadsDefinitions.length; i++)
            {
                this.sprites.push(this.createDoodadSprite(this.doodadsDefinitions[i], mapLayer));
            }
        }
    }

    FieldData.prototype.hide = function()
    {
        this.visible = false;
        for(var i = 0; i < this.sprites.length; i++)
        {
            this.sprites[i].removeFromParent(true);
        }
        this.sprites = [];
    }

    FieldData.prototype.createTileSprite = function (targetLayer) {
        var texture = this.mapData.tileData.getTexture(this.mapData.data[this.y][this.x]);
        var sprite = new cc.Sprite(texture);
        sprite.x = this.x * TILE_SIZE + TILE_SIZE / 2;
        sprite.y = this.y * TILE_SIZE + TILE_SIZE / 2;
        sprite.z = -1000;
        var textureSize = 64;
        sprite.scale = TILE_SIZE / textureSize;
        sprite.setTextureRect(cc.rect(0, 0, textureSize, textureSize));
        targetLayer.addChild(sprite, 0);
        return sprite;
    }

    FieldData.prototype.createDoodadSprite = function(doodad, targetLayer)
    {
        var sprite =    new cc.Sprite(res_path.doodads + "/" + 
                                        doodad.tileset + "/" + doodad.name + ".png");
        sprite.setPosition(doodad.position);
        sprite.setRotation(doodad.rotation);
        sprite.setScale(doodad.scale);
        targetLayer.addChild(sprite, doodad.zIndex);
        return sprite;
    }









    wg.map.FieldData = FieldData;
})();