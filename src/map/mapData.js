(function(){

// map that holds tile and rendering data
var MapData = (function()
{
});
wg.map.MapData = MapData;

// index for rendering (y axis inverted)
MapData.prototype.index = (function(x, y){
    return this.data[this.height - 1 -y][x];
})
MapData.prototype.i = MapData.prototype.index;

MapData.prototype.init = function()
{
    this.height = this.data.length;
    if(this.height == 0)
        this.width = 0;
    else
        this.width = this.data[0].length;

    // init field array
    this.fields = [];
    for(var y = 0; y < this.height; y++)
    {
        var row = [];
        for(var x = 0; x < this.width; x++)
        {
            var newField = new wg.map.FieldData(this, x, y);
            row.push(newField);
        }
        this.fields.push(row);
    }

}

// finalize map generation
MapData.prototype.finalizeTileGeneration = function()
{
    for(var y = 0; y < this.height; y++)
    {
        for(var x = 0; x < this.width; x++)
        {
            this.fields[y][x].textureIndex = this.data[y][x];
        }         
    }
}

// unused atm
//create texture from map data
MapData.prototype.getTexture = function()
{
    var pWidth = this.width;//cc.NextPOT(this.width);     
    var pHeight = this.height;//cc.NextPOT(this.height);

    var bytes = pWidth * pHeight * 4;
    var textureData = new Uint8Array(new ArrayBuffer(bytes));

    // R = top_left, G = top_right, B = bottom_left, A = bottom_right


    for(var y = 0; y < this.height; y += 2)
    {   
        for(var x = 0; x < this.width; x += 2)
        {   // take 4 cells, and convert it to one tile, each cell mapped to one corner
            var textureIndex = ((y / 2 * pWidth) + x/2) * 4; // 2x2 square will be stored in` 1 pixel
            // *4 because 4 ints = 1 pixel (RGBA)
            if(x/2 % 2 == 0 || y/2 % 2 == 0)
            {
                textureData[textureIndex + 0] = 255;this.data[y][x];
            }
            else
            {
                textureData[textureIndex + 1] = 255;this.data[y][x + 1];
            }
            textureData[textureIndex + 2] = this.data[y + 1][x]; 
            textureData[textureIndex + 3] = 255;this.data[y + 1][x + 1];
        }
    }
    var mapTexture = new cc.Texture2D();
    mapTexture.initWithData(textureData, cc.Texture2D.PIXEL_FORMAT_RGBA8888, pWidth, pHeight, new cc.Size(this.width, this.height));
    mapTexture.setAliasTexParameters();
    // update map metadata
    return mapTexture;
}
    
})();