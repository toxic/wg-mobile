(function () {

var MapLayer = cc.Layer.extend({
    sprite: null,
    ctor: function (width, height, doodadLayer) {
        this._super();

        this.tilesX = width;
        this.tilesY = height;
        this.doodadLayer = doodadLayer;

        while(true)
        {
            var generator = new wg.map.MapGenerator(Math.random(), width, height);
            generator.generateMapCellurarAutomaton();
            generator.postProcess(); 

            var entrance = generator.randomInt(3);
            
            if(!generator.createExitEntrance(entrance, 3-entrance))
            {
                continue;
            }
            
            generator.map.finalizeTileGeneration();
            generator.generateDoodads(); 
            generator.map.tileData = new wg.map.TileDefinitions(
            [
                new wg.map.TileDefinition(0, 'jungle/sand.png'),
                new wg.map.TileDefinition(1, 'cave/darkrock8.png'),
                new wg.map.TileDefinition(2, 'forest/forest.png'),
                new wg.map.TileDefinition(3, 'arctic/ice1.png'),
            ]);       
    
            break;
        }

        this.map = generator.map;
        this.entrance = generator.entrance;
        this.exit = generator.exit;
        
        this.visibleRect = cc.rect(0, 0, 0, 0);

        
    }
});

MapLayer.prototype.boundsValid = function(x, y)
{
    return (x >= 0 && y >= 0 && y < this.map.fields.length && x < this.map.fields[0].length);
}

wg.map.MapLayer = MapLayer;

})();