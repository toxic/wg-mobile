(function(){
var TILE_SIZE = wg.c.TILE_SIZE;
var TREE_RADIUS = wg.c.TREE_RADIUS;

var MapLayer = wg.map.MapLayer;

MapLayer.prototype.doodadCollisionCandidates = function(position)
{
    var posX = Math.floor(position.x / TILE_SIZE);
    var posY = Math.floor(position.y / TILE_SIZE);

    var doodads = [];
    for(var y = posY - 1; y <= posY + 1; y++)
    {
        for(var x = posX - 1; x <= posX +1; x++)
        {
            var field = boundsValid( this.map.fields, x, y);
            if(field == undefined)
                continue;
           for(var i = 0; i < field.doodadsDefinitions.length; i++)
           {
               var currentDoodad = field.doodadsDefinitions[i];
                if(doodads.indexOf(currentDoodad) == -1)
                {
                    doodads.push(currentDoodad);
                }
            }
        }
    }
    
    return doodads;
};

var boundsValid = function(twoDimensionalArray, x, y)
{
    var subArray = twoDimensionalArray[y];
    if(subArray == undefined)
        return undefined;
    else
        return subArray[x];
}

})();