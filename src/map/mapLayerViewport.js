(function () {

var MapLayer = wg.map.MapLayer;
MapLayer.prototype.setViewport = function(rect)
{
    var endY = this.visibleRect.y + this.visibleRect.height;
    var endX = this.visibleRect.x + this.visibleRect.width;
    for(var y = this.visibleRect.y; y < endY; y++)
    {
        for(var x = this.visibleRect.x; x < endX; x++)
        {
           if(this.boundsValid(x, y))
            this.map.fields[y][x].hide();
        }         
    }


    endY = rect.y + rect.height;
    endX = rect.x + rect.width;
    for(var y = rect.y; y < endY; y++)
    {
        for(var x = rect.x; x < endX; x++)
        {
           if(this.boundsValid(x, y))
            this.map.fields[y][x].show(this);
        }         
    }

    this.visibleRect = rect;
}

// cc.rect
MapLayer.prototype.updateViewport = function(newRect)
{
    var deltaX = newRect.x - this.visibleRect.x;
    var deltaY = newRect.y - this.visibleRect.y;

    if(deltaX != 0)
    {
        // going right
        if(deltaX > 0)
        {
            var start = this.visibleRect.x + this.visibleRect.width; // show
            var end = this.visibleRect.x + this.visibleRect.width + deltaX;
            for(var y = this.visibleRect.y; y < this.visibleRect.y + this.visibleRect.height; y++)
            {
                for(var x = start; x < end; x++)
                {
                    if(this.boundsValid(x, y))
                        this.map.fields[y][x].show(this);
                }         
            }
            start = this.visibleRect.x; //hide
            end = this.visibleRect.x + deltaX;
            for(var y = this.visibleRect.y; y < this.visibleRect.y + this.visibleRect.height; y++)
            {
                for(var x = start; x < end; x++)
                {
                    if(this.boundsValid(x, y))
                        this.map.fields[y][x].hide();
                }         
            }
        }
        else // deltaX < 0
        {
            var start = this.visibleRect.x + deltaX; // show
            var end = this.visibleRect.x;
            for(var y = this.visibleRect.y; y < this.visibleRect.y + this.visibleRect.height; y++)
            {
                for(var x = start; x < end; x++)
                {
                    if(this.boundsValid(x, y))
                        this.map.fields[y][x].show(this);
                }         
            }
            var start = this.visibleRect.x + this.visibleRect.width + deltaX; // hide
            var end = this.visibleRect.x + this.visibleRect.width;
            for(var y = this.visibleRect.y; y < this.visibleRect.y + this.visibleRect.height; y++)
            {
                for(var x = start; x < end; x++)
                {
                    if(this.boundsValid(x, y))
                        this.map.fields[y][x].hide();
                }         
            }
        }
        this.visibleRect.x = this.visibleRect.x + deltaX;
    }
    else if(deltaY != 0)
    {
        // going right
        if(deltaY > 0)
        {
            var start = this.visibleRect.y + this.visibleRect.height; // show
            var end = this.visibleRect.y + this.visibleRect.height + deltaY;
            for(var y = start; y < end; y++)
            {
                for(var x = this.visibleRect.x; x < this.visibleRect.x + this.visibleRect.width; x++)
                {
                   if(this.boundsValid(x, y))
                        this.map.fields[y][x].show(this);
                }         
            }
            start = this.visibleRect.y; //hide
            end = this.visibleRect.y + deltaY;
            for(var y = start; y < end; y++)
            {
                for(var x = this.visibleRect.x; x < this.visibleRect.x + this.visibleRect.width; x++)
                {
                    if(this.boundsValid(x, y))
                        this.map.fields[y][x].hide();
                }         
            }
        }
        else // deltaY < 0
        {
            var start = this.visibleRect.y + deltaY; // show
            var end = this.visibleRect.y;
            for(var y = start; y < end; y++)
            {
                for(var x = this.visibleRect.x; x < this.visibleRect.x + this.visibleRect.width; x++)
                {
                    if(this.boundsValid(x, y))
                        this.map.fields[y][x].show(this);
                }         
            }
            start = this.visibleRect.y + this.visibleRect.height + deltaY; //hide
            end = this.visibleRect.y + this.visibleRect.height;
            for(var y = start; y < end; y++)
            {
                for(var x = this.visibleRect.x; x < this.visibleRect.x + this.visibleRect.width; x++)
                {
                   if(this.boundsValid(x, y))
                        this.map.fields[y][x].hide();
                }         
            }
        }
        this.visibleRect.y = this.visibleRect.y + deltaY;
    }
}

})();