(function () {

var TILE_SIZE = wg.c.TILE_SIZE;

// def of single tile
var TileDefinition = (function(id, res){
    this.tileID = id;
    this.resource = res;    
});
wg.map.TileDefinition = TileDefinition;

// container for tile def, 
var TileDefinitions = (function(dataArray) {
    this.joinTextures = {};
    this.data = dataArray;
});
wg.map.TileDefinitions = TileDefinitions;

// convert map relative filenam to abs path
TileDefinitions.prototype.tilePath = function (name) {
    return res_path.maps + name;
}

// return texture of tile with id, each id corresponds to one corner
TileDefinitions.prototype.getTexture = function(id1, id2, id3, id4)
{
    if(typeof id2 === "undefined" || (id1 == id2 == id3 == id4)){ // whole tile is made from one texture
        id2 = id1;
        id3 = id1;
        id4 = id1;

        return cc.textureCache.addImage(this.tilePath(this.data[id1].resource));
    }

    var current = this.joinTextures[id1];
    if(current === undefined){
        this.joinTextures[id1] = {};
    }
    current = this.joinTextures[id1][id2];
    if(current === undefined){
        this.joinTextures[id1][id2] = {};
    }
    current = this.joinTextures[id1][id2][id3];
    if(current === undefined){
        this.joinTextures[id1][id2][id3] = {};
    }
    current = this.joinTextures[id1][id2][id3][id4];
    if(current === undefined){
        // prepare image with joins
        var resultTexture = this.makeJoinTexture(id1, id2, id3, id4);
        this.joinTextures[id1][id2][id3][id4] = resultTexture;
        current = resultTexture;
    }

    return current;
}

TileDefinitions.prototype.makeJoinTexture = function(id1, id2, id3, id4)
{
    // todo - load mask image, then create texture from mask and return it
    return cc.textureCache.addImage(this.tilePath(this.data[id1].resource));
}

})();