WG = window.WG || {};

var HelloWorldLayer = cc.Layer.extend({
    sprite:null,
    ctor:function () {
        this._super();

        var size = cc.winSize;

        var that = this;
        /////////////////////////////
        // 3. add your codes below...
        // add a label shows "Hello World"
        // create and initialize a label
//        var helloLabel = new cc.LabelTTF("Hello World", "Arial", 38);
        // position the label on the center of the screen
//        helloLabel.x = size.width / 2;
//        helloLabel.y = 0;
        // add the label as a child to this layer
//        this.addChild(helloLabel, 5);

        // add "HelloWorld" splash screen"
        this.sprite = new cc.Sprite(res.OldMenuMockup_png);
        this.sprite.attr({
            x: size.width / 2,
            y: size.height / 2
//            scale: 1.0
//            rotation: 180
        });
        this.addChild(this.sprite, 0);


        var menu = new cc.Menu();
        menu.x = 0;
        menu.y = 0;
        this.addChild(menu, 2);

        return true;
    }
});

var HelloWorldScene = cc.Scene.extend({
    onEnter:function () {
        this._super();


        var newScene = new wg.game.GameScene();
        cc.director.runScene(newScene);
    }
});