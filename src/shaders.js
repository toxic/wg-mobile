var shaderCode = 
{
    identityFrag : "\
        varying vec4 v_fragmentColor;     \
        varying vec2 v_texCoord;           \
        \
        void main()         \
        {                       \
            vec4 v_orColor = v_fragmentColor * texture2D(CC_Texture0, v_texCoord);  \
            float gray = dot(v_orColor.rgb, vec3(0.299, 0.587, 0.114));             \
            gl_FragColor = vec4(gray, gray, gray, v_orColor.a);                     \
        }                                                                           \
",
    identityVert : "\
attribute vec4 a_position;      \
attribute vec2 a_texCoord;      \
attribute vec4 a_color;         \
                                \
varying vec4 v_fragmentColor;   \
varying vec2 v_texCoord;        \
                                \
void main()                     \
{                               \
    gl_Position = CC_PMatrix * CC_MVMatrix * a_position;        \
    v_fragmentColor = a_color;     \
    v_texCoord = a_texCoord;       \
}                               \
"

}

var example_rendertarget_with_shader = function()
{
        var s_fire = cc.textureCache.addImage('res/assets/maps/tile_masks/mask1.png' );
        var sprite1 = new cc.Sprite(s_fire);
        sprite1.x = TileSize/2;
        sprite1.y = TileSize/2;

        var renderTexture = new cc.RenderTexture(TileSize, TileSize);
        renderTexture.x = 512;
        renderTexture.y = 512;
        renderTexture.clearColorVal = cc.color(0, 0, 0, 255);
        renderTexture.clearFlags = cc._renderContext.COLOR_BUFFER_BIT;
        renderTexture.begin();
    

        var shader = new cc.GLProgram();
        shader.initWithVertexShaderByteArray(shaderCode.identityVert, shaderCode.identityFrag);
        shader.retain();
        shader.addAttribute(cc.ATTRIBUTE_NAME_POSITION, cc.VERTEX_ATTRIB_POSITION);
        shader.addAttribute(cc.ATTRIBUTE_NAME_COLOR, cc.VERTEX_ATTRIB_COLOR);
        shader.addAttribute(cc.ATTRIBUTE_NAME_TEX_COORD, cc.VERTEX_ATTRIB_TEX_COORDS);
        shader.link();
        shader.updateUniforms();
        sprite1.setShaderProgram(shader);
        sprite1.visit();
        renderTexture.end();
}














