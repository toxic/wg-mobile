"use strict";

var wg = {};
window.wg = wg;
wg.map = {}; // map display/loading/generation
wg.util = {}; 
wg.game = {};    
wg.logic = {};

// configuration, file config.js
wg.c = {};


wg.namespace = function() {
    var obj = null, i, parts;
    var parts = arguments[0].split(".");
    obj = window;
    for (i = 0; i < parts.length; i++) {
       obj[parts[i]] = obj[parts[i]] || {};
       obj = obj[parts[i]];
    }
    return obj;
};