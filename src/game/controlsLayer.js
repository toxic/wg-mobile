wg.namespace("wg.game");

//a moze tutaj powinno byc cos w stylu:
// require(wg.game.controls.Button)
// require(wg.game.controls.Joystick) ?

(function() {
    //bez sensu ze to wystawiamy na publiczny namespace:
    wg.game.ControlsLayer = cc.Layer.extend({
        ctor:function () {
            this._super();
        }
    });
})();

//wywalic do pliku np src/game/controls/costamManager.js
wg.namespace("wg.game.controls");

(function() {
    wg.game.controls.setup = function setupControls() {
        var size = cc.winSize;

        wg.game.controls.movementJoystick = new wg.game.controls.Joystick({
            center: cc.p(136, 136)
        });
        wg.game.controls.buttonA = new wg.game.controls.Button({
            center: cc.p(size.width - 136, 212),
            key: cc.KEY.q,
            onPress: function() {
                wg.logic.characterController.startAbility(0);
            },
            onRelease: function() {
                wg.logic.characterController.stopAbility(0);
            }
        });
        wg.game.controls.buttonB = new wg.game.controls.Button({
            center: cc.p(size.width - 212, 136),
            key: cc.KEY.w,
            onPress: function() {
                wg.logic.characterController.startAbility(1);
            },
            onRelease: function() {
                wg.logic.characterController.stopAbility(1);
            }
        });
        wg.game.controls.buttonC = new wg.game.controls.Button({
            center: cc.p(size.width - 64, 136),
            key: cc.KEY.e,
            onPress: function() {
                wg.logic.characterController.startAbility(2);
            },
            onRelease: function() {
                wg.logic.characterController.stopAbility(2);
            }
        });
        wg.game.controls.buttonD = new wg.game.controls.Button({
            center: cc.p(size.width - 136, 64),
            key: cc.KEY.r,
            onRelease: function() {
                //temporary, for development:
                var demonPosition = cc.p(wg.logic.player.position);
                var direction = wg.logic.player.getForward();
                cc.pAddIn(demonPosition, cc.pMult(direction, 100));
                wg.logic.agents.create(demonPosition.x, demonPosition.y);
            }
        });
    }
})();
/*            //to trzeba gdzies wywalic, np do playerManager.js
            var animationActions = {
                movement: new cc.Animate(new cc.Animation([
                    new cc.SpriteFrame(res.player, cc.rect(64 * 0, 0, 64, 64)),
                    new cc.SpriteFrame(res.player, cc.rect(64 * 1, 0, 64, 64)),
                    new cc.SpriteFrame(res.player, cc.rect(64 * 2, 0, 64, 64)),
                    new cc.SpriteFrame(res.player, cc.rect(64 * 3, 0, 64, 64)),
                    new cc.SpriteFrame(res.player, cc.rect(64 * 4, 0, 64, 64)),
                    new cc.SpriteFrame(res.player, cc.rect(64 * 5, 0, 64, 64)),
                    new cc.SpriteFrame(res.player, cc.rect(64 * 6, 0, 64, 64)),
                    new cc.SpriteFrame(res.player, cc.rect(64 * 7, 0, 64, 64))
                ], 0.1)),
                strafe: new cc.Animate(new cc.Animation([
                    new cc.SpriteFrame(res.player, cc.rect(64 * 0, 64, 64, 64)),
                    new cc.SpriteFrame(res.player, cc.rect(64 * 1, 64, 64, 64)),
                    new cc.SpriteFrame(res.player, cc.rect(64 * 2, 64, 64, 64)),
                    new cc.SpriteFrame(res.player, cc.rect(64 * 3, 64, 64, 64)),
                    new cc.SpriteFrame(res.player, cc.rect(64 * 4, 64, 64, 64)),
                    new cc.SpriteFrame(res.player, cc.rect(64 * 5, 64, 64, 64)),
                    new cc.SpriteFrame(res.player, cc.rect(64 * 6, 64, 64, 64)),
                    new cc.SpriteFrame(res.player, cc.rect(64 * 7, 64, 64, 64))
                ], 0.1)),
                wobble: new cc.Animate(new cc.Animation([
                    new cc.SpriteFrame(res.player, cc.rect(64 * 0, 64 * 2, 64, 64)),
                    new cc.SpriteFrame(res.player, cc.rect(64 * 1, 64 * 2, 64, 64)),
                    new cc.SpriteFrame(res.player, cc.rect(64 * 2, 64 * 2, 64, 64)),
                    new cc.SpriteFrame(res.player, cc.rect(64 * 3, 64 * 2, 64, 64)),
                    new cc.SpriteFrame(res.player, cc.rect(64 * 4, 64 * 2, 64, 64)),
                    new cc.SpriteFrame(res.player, cc.rect(64 * 5, 64 * 2, 64, 64)),
                    new cc.SpriteFrame(res.player, cc.rect(64 * 6, 64 * 2, 64, 64)),
                    new cc.SpriteFrame(res.player, cc.rect(64 * 7, 64 * 2, 64, 64))
                ], 0.1)),
                castSpell: {
                    default: new cc.Animate(new cc.Animation([
                        new cc.SpriteFrame(res.player, cc.rect(64 * 0, 64 * 3, 64, 64)),
                        new cc.SpriteFrame(res.player, cc.rect(64 * 1, 64 * 3, 64, 64)),
                        new cc.SpriteFrame(res.player, cc.rect(64 * 2, 64 * 3, 64, 64)),
                        new cc.SpriteFrame(res.player, cc.rect(64 * 3, 64 * 3, 64, 64)),
                        new cc.SpriteFrame(res.player, cc.rect(64 * 4, 64 * 3, 64, 64)),
                        new cc.SpriteFrame(res.player, cc.rect(64 * 5, 64 * 3, 64, 64)),
                        new cc.SpriteFrame(res.player, cc.rect(64 * 6, 64 * 3, 64, 64)),
                        new cc.SpriteFrame(res.player, cc.rect(64 * 7, 64 * 3, 64, 64))
                     ], 0.1)),
                     oneHand: cc.animate(new cc.Animation([
                        new cc.SpriteFrame(res.player, cc.rect(64 * 0, 64 * 4, 64, 64)),
                        new cc.SpriteFrame(res.player, cc.rect(64 * 1, 64 * 4, 64, 64)),
                        new cc.SpriteFrame(res.player, cc.rect(64 * 2, 64 * 4, 64, 64)),
                        new cc.SpriteFrame(res.player, cc.rect(64 * 3, 64 * 4, 64, 64)),
                        new cc.SpriteFrame(res.player, cc.rect(64 * 4, 64 * 4, 64, 64)),
                    ], 0.1)),
                    bothHands: new cc.Animate(new cc.Animation([
                        new cc.SpriteFrame(res.player, cc.rect(64 * 0, 64 * 5, 64, 64)),
                        new cc.SpriteFrame(res.player, cc.rect(64 * 1, 64 * 5, 64, 64)),
                        new cc.SpriteFrame(res.player, cc.rect(64 * 2, 64 * 5, 64, 64)),
                        new cc.SpriteFrame(res.player, cc.rect(64 * 3, 64 * 5, 64, 64)),
                        new cc.SpriteFrame(res.player, cc.rect(64 * 4, 64 * 5, 64, 64)),
                    ], 0.1))
                },
                death: new cc.Animate(new cc.Animation([
                    new cc.SpriteFrame(res.player, cc.rect(64 * 0, 64 * 7, 64, 64)),
                    new cc.SpriteFrame(res.player, cc.rect(64 * 1, 64 * 7, 64, 64)),
                    new cc.SpriteFrame(res.player, cc.rect(64 * 2, 64 * 7, 64, 64)),
                    new cc.SpriteFrame(res.player, cc.rect(64 * 3, 64 * 7, 64, 64)),
                    new cc.SpriteFrame(res.player, cc.rect(64 * 4, 64 * 7, 64, 64)),
                    new cc.SpriteFrame(res.player, cc.rect(64 * 5, 64 * 7, 64, 64)),
                    new cc.SpriteFrame(res.player, cc.rect(64 * 6, 64 * 7, 64, 64)),
                    new cc.SpriteFrame(res.player, cc.rect(64 * 7, 64 * 7, 64, 64))
                ], 0.1))
            };
            //to trzeba jakos ladniej potem zrobic:
            animationActions.movement.retain();
            animationActions.strafe.retain();
            animationActions.wobble.retain();
            animationActions.castSpell.default.retain();
            animationActions.castSpell.oneHand.retain();
            animationActions.castSpell.bothHands.retain();
            animationActions.death.retain();
*/
