wg.namespace("wg.game");

(function () {

    var GameLayer = cc.Layer.extend({
        ctor: function () {
            this._super();


            var doodadLayer = new cc.Layer();
            var width = 100;
            var height = 100;
            this.mapLayer = new wg.map.MapLayer(width, height, doodadLayer);


            this.collisionManager = new wg.logic.physics.CollisionManager(
                width * wg.c.TILE_SIZE,
                height * wg.c.TILE_SIZE,
                wg.c.COLLISION_GRID_SIZE
            );
            
            this.addChild(this.mapLayer, 0);
            this.addChild(doodadLayer, 10);


            this.camera = new wg.game.camera(this);
            this.mapLayer.setViewport(this.camera.getVisibleRect());
        }
    });

    wg.game.GameLayer = GameLayer;

})();