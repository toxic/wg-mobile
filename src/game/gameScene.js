wg.namespace("wg.game");

(function () {
    var GameScene = cc.Scene.extend({
        onEnter: function () {
            this._super();

            var gameLayer = new wg.game.GameLayer();
            wg.game.gameLayer = gameLayer;
            wg.game.mapLayer = gameLayer.mapLayer; //todo: replace gameLayer with mapLayer completely
            this.addChild(gameLayer);
            var controlsLayer = new wg.game.ControlsLayer();
            wg.game.controlsLayer = controlsLayer;
            this.addChild(controlsLayer);

            wg.game.controls.setup();

            var entrance = gameLayer.mapLayer.entrance;
            
            var player = wg.logic.players.create(entrance[0], entrance[1]);
            wg.logic.characterController.attach(player);

            gameLayer.scheduleUpdate();
            gameLayer.update = wg.logic.update;

        }
    });

    wg.game.GameScene = GameScene;

})();