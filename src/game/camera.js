wg.namespace("wg.game");

(function() {

var halfX;
var halfY;
var TILE_SIZE = wg.c.TILE_SIZE;
wg.game.camera = function(gameLayer)
{
    this.gameLayer = gameLayer;
    this.mapLayer = gameLayer.mapLayer;

    this.bounds = new cc.p(this.mapLayer.tilesX * TILE_SIZE, this.mapLayer.tilesY * TILE_SIZE);
    this.size = cc.winSize;
    this.offset = cc.p(this.size.width/2, this.size.height/2);
    this.position = new cc.p(this.size.width/2, this.size.height/2);

    halfX = this.size.width/2;
    halfY = this.size.height/2;
}


wg.game.camera.prototype.getVisibleRect = function()
{
    var x = (this.position.x - halfX) / TILE_SIZE;
    var y = (this.position.y - halfY) / TILE_SIZE;

    var width = ((this.position.x + halfX) / TILE_SIZE) - x;
    var height = ((this.position.y + halfY) / TILE_SIZE) - y;

    // do testów, todo: remove
    x -= 2;
    y -= 2;
    width += 4;
    height += 4;

    var func = Math.round;
    return cc.rect(func(x), func(y), func(width), func(height));
}

wg.game.camera.prototype.getPosition = function()
{
    return this.position;
}

wg.game.camera.prototype.setPosition = function(newPosition)
{
    var x = newPosition.x;
    var y = newPosition.y;
    
    // validate bounds
    if(x < halfX)
    {
        x = halfX;
    }

    if(y < halfY)
    {
        y = halfY;
    }

    if(x > this.bounds.x - halfX)
    {
        x = this.bounds.x - halfX;
    }

    if(y > this.bounds.y - halfY)
    {
        y = this.bounds.y - halfY;
    }

    this.position = new cc.p(x, y);
}

wg.game.camera.prototype.update = function(deltaTime)
{
    //let the camera know how to update itself (i.e. focus on player)
    var player = wg.logic.characterController.player;
    this.setPosition(player.position);

    this.mapLayer.updateViewport(this.getVisibleRect());
    this.gameLayer.setPosition(cc.pMult(cc.pSub(this.position, this.offset), -1));
}

})();