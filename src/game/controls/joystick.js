wg.namespace("wg.game.controls");

(function() {

    wg.game.controls.Joystick = function(params) {
        var joystick = this;
        var layer = wg.game.controlsLayer;
        joystick.center = params.center || cc.p(128, 128);
        
        joystick.maxRadius = 80;
        joystick.value = cc.p(0, 0);
        joystick.base = new cc.Sprite(res.joystick_base);
        joystick.base.setPosition(joystick.center);
        joystick.stick = new cc.Sprite(res.joystick_stick);
        joystick.stick.setPosition(joystick.base);
        layer.addChild(joystick.base, 100);
        layer.addChild(joystick.stick, 100);

        /* this was needed for development only
        joystick.remove = function() {
            layer.removeChild(this.base);
            layer.removeChild(this.stick);
        };*/

        joystick.onTouchBegan = function (touch, event) {
            var triggerArea = cc.rect(
                joystick.center.x - 196,
                joystick.center.y - 196,
                196 * 2,
                196 * 2
            );
            if (cc.rectContainsPoint(triggerArea, touch.getLocation())) {
                joystick.onTouchMoved(touch, event);
                return true;
            } else {
                return false;
            }
        };
        joystick.onTouchMoved = function (touch, event) {
            var target = event.getCurrentTarget();
            var touchPoint = touch.getLocation();

            var delta = cc.pSub(touchPoint, joystick.center);
            var radius = cc.pLength(delta);
            if (radius > joystick.maxRadius) {
                cc.pNormalizeIn(delta);
                cc.pMultIn(delta, joystick.maxRadius);
            }

            joystick.value = cc.pMult(delta, 1 / joystick.maxRadius);
            joystick.stick.setPosition(cc.pAdd(delta, joystick.center));
        };
        joystick.onTouchEnded = function (touch, event) {
            joystick.stick.setPosition(joystick.center);
            joystick.value = cc.p(0, 0);
        };

        cc.eventManager.addListener({
            event: cc.EventListener.TOUCH_ONE_BY_ONE,
            swallowTouches: true,
            onTouchBegan: joystick.onTouchBegan,
            onTouchMoved: joystick.onTouchMoved,
            onTouchEnded: joystick.onTouchEnded
        }, joystick.stick);

        cc.eventManager.addListener({
            value: cc.p(0, 0),
            mapping: {
                up: cc.p(0, 1),
                down: cc.p(0, -1),
                left: cc.p(-1, 0),
                right: cc.p(1, 0)
            },
            pressedKeys: {},
            event: cc.EventListener.KEYBOARD,
            onKeyPressed: function(keyCode, event) {
                if (this.pressedKeys[keyCode]) return;
                this.pressedKeys[keyCode] = true;

                if (keyCode === cc.KEY["up"]) {
                    cc.pAddIn(this.value, this.mapping.up);
                } else if (keyCode === cc.KEY["down"]) {
                    cc.pAddIn(this.value, this.mapping.down);
                } else if (keyCode === cc.KEY["left"]) {
                    cc.pAddIn(this.value, this.mapping.left);
                } else if (keyCode === cc.KEY["right"]) {
                    cc.pAddIn(this.value, this.mapping.right);
                }
                this._updateJoystickValue();
            },
            onKeyReleased: function(keyCode, event) {
                this.pressedKeys[keyCode] = false;
                if (keyCode === cc.KEY["up"]) {
                    cc.pSubIn(this.value, this.mapping.up);
                } else if (keyCode === cc.KEY["down"]) {
                    cc.pSubIn(this.value, this.mapping.down);
                } else if (keyCode === cc.KEY["left"]) {
                    cc.pSubIn(this.value, this.mapping.left);
                } else if (keyCode === cc.KEY["right"]) {
                    cc.pSubIn(this.value, this.mapping.right);
                }
                this._updateJoystickValue();
            },
            _updateJoystickValue: function() {
                joystick.value = cc.pNormalize(this.value);
            }
        }, joystick.stick);
    }
})();
