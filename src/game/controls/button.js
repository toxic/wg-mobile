wg.namespace("wg.game.controls");

// na dobra sprawe to ja tu nie chce namespace'a wg.game.controls'
// tylko bardziej cos w rodzaju define/require...

(function() {
    var _buttons = [];

    wg.game.controls.Button = function(params) {
        var button = this;
        _buttons.push(button);
        button.center = params.center || cc.p(0, 0);
        button.onPress = params.onPress;
        button.onRelease = params.onRelease;
        button.keyCode = params.key;

        button.base = new cc.Sprite(res.button_base);
        button.base.setScale(0.75);
        button.base.setPosition(button.center);
        wg.game.controlsLayer.addChild(button.base, 100);

        /* this was needed for development only
        button.remove = function() {
            wg.game.controlsLayer.removeChild(button.base);
            wg.game.controlsLayer.removeChild(button.stick);
        };*/

        button.down = function() {
            button.base.setTexture(res.button_base_pressed);
            if (button.onPress) {
                button.onPress();
            }
        };
        button.up = function() {
            button.base.setTexture(res.button_base);
            if (button.onRelease) {
                button.onRelease();
            }
        };

        button.onTouchBegan = function (touch, event) {
            var location = touch.getLocation();
            if (getClosestButton(location, button) != button) {
                return false;
            }
            var triggerArea = cc.rect(
                button.center.x - 96,
                button.center.y - 96,
                96 * 2,
                96 * 2
            );
            if (cc.rectContainsPoint(triggerArea, location)) {
                button.down();
                return true;
            } else {
                return false;
            }
        };
        button.onTouchMoved = function (touch, event) {
            var target = event.getCurrentTarget();
            var touchPoint = touch.getLocation();
        };
        button.onTouchEnded = function (touch, event) {
            button.up();
        };

        cc.eventManager.addListener({
            event: cc.EventListener.TOUCH_ONE_BY_ONE,
            swallowTouches: true,
            onTouchBegan: button.onTouchBegan,
            onTouchMoved: button.onTouchMoved,
            onTouchEnded: button.onTouchEnded
        }, button.base);

        if (button.keyCode && 'keyboard' in cc.sys.capabilities) {
            cc.eventManager.addListener({
                event: cc.EventListener.KEYBOARD,
                onKeyPressed: function (key, event) {
                    if (key != button.keyCode) {
                        return;
                    }
                    button.down();
                },
                onKeyReleased: function (key, event) {
                    if (key != button.keyCode) {
                        return;
                    }
                    button.up();
                }
            }, button.base);
        }
    }
    function getClosestButton(position, button) {
        var closestButton = button;
        for (var i = 0; i < _buttons.length; i++) {
            if (cc.pDistanceSQ(position, _buttons[i].center) < cc.pDistanceSQ(position, closestButton.center)) {
                closestButton = _buttons[i];
            }
        }
        return closestButton;
    }
})();
